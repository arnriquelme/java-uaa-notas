package com.uaanotas.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.uaanotas.beans.Materia;

public class MateriaDao {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public int save(Materia p) {
		String sql = "insert into materia(descripcion) values('" + p.getDescripcion()+ "')";
		return template.update(sql);
	}

	public int update(Materia p) {
		String sql = "update materia set descripcion='" + p.getDescripcion() + "' where id=" + p.getId() + "";
		return template.update(sql);
	}

	public int delete(int id) {
		String sql = "delete from materia where id=" + id + "";
		return template.update(sql);
	}

	public Materia getMateriaById(int id) {
		String sql = "select * from materia where id=?";
		return template.queryForObject(sql, new Object[] { id }, new BeanPropertyRowMapper<Materia>(Materia.class));
	}

	public List<Materia> getMaterias() {
		return template.query("select * from materia", new RowMapper<Materia>() {
			public Materia mapRow(ResultSet rs, int row) throws SQLException {
				Materia e = new Materia();
				e.setId(rs.getInt(1));
				e.setDescripcion(rs.getString(2));
				
				return e;
			}
		});
	}
}
