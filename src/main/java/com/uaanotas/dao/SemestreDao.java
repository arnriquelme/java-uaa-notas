package com.uaanotas.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.uaanotas.beans.Semestre;

public class SemestreDao {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public int save(Semestre p) {
		String sql = "insert into semestre(descripcion) values('" + p.getDescripcion()+ "')";
		return template.update(sql);
	}

	public int update(Semestre p) {
		String sql = "update semestre set descripcion='" + p.getDescripcion() + "' where id=" + p.getId() + "";
		return template.update(sql);
	}

	public int delete(int id) {
		String sql = "delete from semestre where id=" + id + "";
		return template.update(sql);
	}

	public Semestre getSemestreById(int id) {
		String sql = "select * from semestre where id=?";
		return template.queryForObject(sql, new Object[] { id }, new BeanPropertyRowMapper<Semestre>(Semestre.class));
	}

	public List<Semestre> getSemestres() {
		return template.query("select * from semestre", new RowMapper<Semestre>() {
			public Semestre mapRow(ResultSet rs, int row) throws SQLException {
				Semestre e = new Semestre();
				e.setId(rs.getInt(1));
				e.setDescripcion(rs.getString(2));
				
				return e;
			}
		});
	}
}
