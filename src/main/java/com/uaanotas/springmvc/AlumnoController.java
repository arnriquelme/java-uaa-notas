package com.uaanotas.springmvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.uaanotas.beans.Alumno;
import com.uaanotas.dao.AlumnoDao;

@Controller
public class AlumnoController {
	@Autowired
	AlumnoDao daoAlumno;

	@RequestMapping("/alumnoform")
	public ModelAndView showform() {
		return new ModelAndView("alumnoform", "command", new Alumno());
	}

	@RequestMapping(value = "/savealumno", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("alumno") Alumno alumno) {
		daoAlumno.save(alumno);
		return new ModelAndView("redirect:/viewalumno");
	}

	@RequestMapping("/viewalumno")
	public ModelAndView viewalumno() {
		List<Alumno> list = daoAlumno.getAlumnos();
		return new ModelAndView("viewalumno", "list", list);
	}

	@RequestMapping(value = "/editalumno/{id}")
	public ModelAndView edit(@PathVariable int id) {
		Alumno alumno = daoAlumno.getAlumnoById(id);
		return new ModelAndView("alumnoeditform", "command", alumno);
	}

	@RequestMapping(value = "/editsavealumno", method = RequestMethod.POST)
	public ModelAndView editsave(@ModelAttribute("alumno") Alumno alumno) {
		daoAlumno.update(alumno);
		return new ModelAndView("redirect:/viewalumno");
	}

	@RequestMapping(value = "/deletealumno/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable int id) {
		daoAlumno.delete(id);
		return new ModelAndView("redirect:/viewalumno");
	}

}