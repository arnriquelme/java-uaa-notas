package com.uaanotas.springmvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.uaanotas.beans.Curso;
import com.uaanotas.dao.CursoDao;

@Controller
public class CursoController {
	@Autowired
	CursoDao daoCurso;

	@RequestMapping("/cursoform")
	public ModelAndView showform() {
		return new ModelAndView("cursoform", "command", new Curso());
	}

	@RequestMapping(value = "/savecurso", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("curso") Curso curso) {
		daoCurso.save(curso);
		return new ModelAndView("redirect:/viewcurso");
	}

	@RequestMapping("/viewcurso")
	public ModelAndView viewcurso() {
		List<Curso> list = daoCurso.getCursos();
		return new ModelAndView("viewcurso", "list", list);
	}

	@RequestMapping(value = "/editcurso/{id}")
	public ModelAndView edit(@PathVariable int id) {
		Curso curso = daoCurso.getCursoById(id);
		return new ModelAndView("cursoeditform", "command", curso);
	}

	@RequestMapping(value = "/editsavecurso", method = RequestMethod.POST)
	public ModelAndView editsave(@ModelAttribute("curso") Curso curso) {
		daoCurso.update(curso);
		return new ModelAndView("redirect:/viewcurso");
	}

	@RequestMapping(value = "/deletecurso/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable int id) {
		daoCurso.delete(id);
		return new ModelAndView("redirect:/viewcurso");
	}

}