package com.uaanotas.springmvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.uaanotas.beans.Materia;
import com.uaanotas.dao.MateriaDao;

@Controller
public class MateriaController {
	@Autowired
	MateriaDao daoMateria;

	@RequestMapping("/materiaform")
	public ModelAndView showform() {
		return new ModelAndView("materiaform", "command", new Materia());
	}

	@RequestMapping(value = "/savemateria", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("materia") Materia materia) {
		daoMateria.save(materia);
		return new ModelAndView("redirect:/viewmateria");
	}

	@RequestMapping("/viewmateria")
	public ModelAndView viewmateria() {
		List<Materia> list = daoMateria.getMaterias();
		return new ModelAndView("viewmateria", "list", list);
	}

	@RequestMapping(value = "/editmateria/{id}")
	public ModelAndView edit(@PathVariable int id) {
		Materia materia = daoMateria.getMateriaById(id);
		return new ModelAndView("materiaeditform", "command", materia);
	}

	@RequestMapping(value = "/editsavemateria", method = RequestMethod.POST)
	public ModelAndView editsave(@ModelAttribute("materia") Materia materia) {
		daoMateria.update(materia);
		return new ModelAndView("redirect:/viewmateria");
	}

	@RequestMapping(value = "/deletemateria/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable int id) {
		daoMateria.delete(id);
		return new ModelAndView("redirect:/viewmateria");
	}

}